package cnscard.cns.com.vn.cardapi;

import android.util.Log;

import cnscard.cns.com.vn.cardapi.utils.ByteUtils;
import cnscard.cns.com.vn.cardapi.utils.Hex;

public class PocketData extends PocketLog {
    //Class status
    private boolean InfoInit = false;
    private boolean ValueInit = false;
    private boolean LogInit = false;

    // Pocket info: 26 bytes
    /** Pocket Account Number: 8 | Pocket type: 1 | Pocket effective date: 4 | Pocket expire date: 4 | Pocket status: 1 | Max: 4 | Min: 4 **/
    private String pocketAccNum, pocketType, pocketEffectiveDate, pocketExpireDate, pocketStatus;
    private int maxBalance = 0, minBalance = 0;
    // Pocket Value: 4 bytes
    private int pocketBalance = 0;

    //    // Pocket Log: 31 bytes
//    /** Transaction Code: 1 | Counter: 4 | Datetime: 7 | Before: 4 | Used: 4 | After: 4 | SAM ID: 7 **/
//    private String transactionCode, transactionDateTime;
//    private int transactionCounter;
//    private int beforeBalance = 0, usedAmount = 0, afterBalance = 0;
//    private String samID;

    public PocketData() {
//        super();
        this.pocketAccNum = "";
        this.pocketType = "";
        this.pocketEffectiveDate = "";
        this.pocketExpireDate = "";
        this.pocketStatus = "";

//        // Log info
//        this.transactionCode = "00";
//        // Transaction counter's place
//        this.transactionDateTime = "00000000000000";
//        // Before balance's place
//        // Used amount's place
//        // After balance's place
//        this.samID = "00000000000000";
    }

    public void pocketInfoInit(String data) {
        if (data.length()==52 /*(26x2)*/) {
            // 0  -> (8x2): 16
            this.pocketAccNum = data.substring(0,16);
            // 16 -> (8+1)x2: 18
            this.pocketType = data.substring(2,4);
            // 18 -> (8+1+4)x2: 26
            this.pocketEffectiveDate = data.substring(18,26);
            // 26 -> (8+1+4+4)x2: 34
            this.pocketExpireDate = data.substring(26,34);
            // 34 -> (8+1+4+4+1)x2: 36
            this.pocketStatus = data.substring(34,36);
            // 36 -> (8+1+4+4+1+4)x2: 44
            this.maxBalance = ByteUtils.hexString2Int(data.substring(36,44));
            // 44 -> (8+1+4+4+1+4+4)x2: 52
            this.minBalance = ByteUtils.hexString2Int(data.substring(44,52));

            this.InfoInit = true;
        }
        else {
            this.InfoInit = false;
        }
    }

    public String getPocketAccNum() {
        return pocketAccNum;
    }

    public String getPocketType() {
        return pocketType;
    }

    public String getPocketEffectiveDate() {
        return pocketEffectiveDate;
    }

    public String getPocketExpireDate() {
        return pocketExpireDate;
    }

    public String getPocketStatus() {
        return pocketStatus;
    }

    public int getMaxBalance() {
        return maxBalance;
    }

    public int getMinBalance() {
        return minBalance;
    }

    public int getPocketBalance() {
        return pocketBalance;
    }

    public void setPocketBalance(int pocketBalance) {
        this.pocketBalance = pocketBalance;
    }
}
