package cnscard.cns.com.vn.cardapi.utils;

public final class Hex {
    private static char[] hexDigit = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    public static String encode(byte[] data) {
        StringBuilder hexString = new StringBuilder();
        for (byte b: data) {
            int actualByte = b & 0xff;
            char bh = hexDigit[(actualByte >>> 4)];
            char bl = hexDigit[(actualByte & 0xF)];
//            hexString.append(bh).append(bl).append(' ');
            hexString.append(bh).append(bl);
        }

        return hexString.toString();
    }

    public static byte[] decode(String hexString) {
        if (hexString == null) {
            throw new IllegalArgumentException("The input must be not null.");
        } else if (hexString.isEmpty()) {
            return new byte[0];
        } else {
            hexString = hexString.replace(" ", "");
            hexString = hexString.toUpperCase();
            byte[] ret = new byte[hexString.length() / 2];

            int j = 0;
            for (int i = 0; i < hexString.length(); i += 2) {
                byte b1= (byte) (hexString.charAt(i)   -'0'); if (b1>9) b1 -= 7;
                byte b2= (byte) (hexString.charAt(i + 1) -'0'); if (b2>9) b2 -= 7;
                ret[j++] = (byte) ((b1<<4) + b2);
            }

            return ret;
        }
    }

    public static String complementString(String hexString) {
        byte[] hexArray = Hex.decode(hexString);
        for (int i = 0; i < hexArray.length; i++) {
            hexArray[i] ^= 0xFF;
        }
        return Hex.encode(hexArray);
    }
}

