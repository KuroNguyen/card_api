package cnscard.cns.com.vn.cardapi;

public class Booking {
    private String orderStatus;
    private String orderNumber;
    private String productID1, producQuan1;
    private String productID2, producQuan2;
    private String productID3, producQuan3;
    private String productID4, producQuan4;
    private String bookingDeviceID;
    private String deliveryDeviceID;
    private String transacCounter;
    private String orderDatetime;

    public Booking (String booking) {
        this.orderStatus = booking.substring(0,1*2);
        this.orderNumber = booking.substring(1*2,5*2);
        this.productID1 = booking.substring(5*2,6*2);
        this.producQuan1 = booking.substring(6*2,7*2);
        this.productID2 = booking.substring(7*2,8*2);
        this.producQuan2 = booking.substring(8*2,9*2);
        this.productID3 = booking.substring(9*2,10*2);
        this.producQuan3 = booking.substring(10*2,11*2);
        this.productID4 = booking.substring(11*2,12*2);
        this.producQuan4 = booking.substring(12*2,13*2);
        this.bookingDeviceID = booking.substring(13*2,17*2);
        this.deliveryDeviceID = booking.substring(17*2,21*2);
        this.transacCounter = booking.substring(21*2,25*2);
        this.orderDatetime = booking.substring(25*2,32*2);
    }

    public String bookingSerialize() {
        String s = getOrderStatus() + getOrderNumber() + getProductID1() + getProducQuan1() +
                getProductID2() + getProducQuan2() + getProductID3() + getProducQuan3() +
                getProductID4() + getProducQuan4() + getBookingDeviceID() + getDeliveryDeviceID() +
                getTransacCounter() + getOrderDatetime();
        return s;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getProductID1() {
        return productID1;
    }

    public void setProductID1(String productID1) {
        this.productID1 = productID1;
    }

    public String getProducQuan1() {
        return producQuan1;
    }

    public void setProducQuan1(String producQuan1) {
        this.producQuan1 = producQuan1;
    }

    public String getProductID2() {
        return productID2;
    }

    public void setProductID2(String productID2) {
        this.productID2 = productID2;
    }

    public String getProducQuan2() {
        return producQuan2;
    }

    public void setProducQuan2(String producQuan2) {
        this.producQuan2 = producQuan2;
    }

    public String getProductID3() {
        return productID3;
    }

    public void setProductID3(String productID3) {
        this.productID3 = productID3;
    }

    public String getProducQuan3() {
        return producQuan3;
    }

    public void setProducQuan3(String producQuan3) {
        this.producQuan3 = producQuan3;
    }

    public String getProductID4() {
        return productID4;
    }

    public void setProductID4(String productID4) {
        this.productID4 = productID4;
    }

    public String getProducQuan4() {
        return producQuan4;
    }

    public void setProducQuan4(String producQuan4) {
        this.producQuan4 = producQuan4;
    }

    public String getBookingDeviceID() {
        return bookingDeviceID;
    }

    public void setBookingDeviceID(String bookingDeviceID) {
        this.bookingDeviceID = bookingDeviceID;
    }

    public String getDeliveryDeviceID() {
        return deliveryDeviceID;
    }

    public void setDeliveryDeviceID(String deliveryDeviceID) {
        this.deliveryDeviceID = deliveryDeviceID;
    }

    public String getTransacCounter() {
        return transacCounter;
    }

    public void setTransacCounter(String transacCounter) {
        this.transacCounter = transacCounter;
    }

    public String getOrderDatetime() {
        return orderDatetime;
    }

    public void setOrderDatetime(String orderDatetime) {
        this.orderDatetime = orderDatetime;
    }
}
