package cnscard.cns.com.vn.cardapi;

import java.io.IOException;

public interface ICnsSmartCard {
    public static ICnsSmartCard getInstance(ICardReader nfcReader, ICardReader samReader) throws IOException {
        return new CnsSmartCardImpl(nfcReader, samReader);
    }

    /**
     * @api {Void} cardInit() Initial card
     * @apiGroup Card
     * @apiName cardInit
     * @apiDescription This function need to be called when the card tapped in the first time.
     * @apiExample Example usage:
     *      CnsSmartCard.cardInit();
     * @throws IOException
     */
    void cardInit() throws IOException;

    /**
     * @api {Integer} getBalance() Get balance
     * @apiGroup Card
     * @apiName getBalance
     * @apiDescription This function is used to get the balance of the card
     * @apiExample {java} Example usage:
     *      int balance = CnsSmartCard.getBalance();
     * @return
     * @throws IOException
     */
    int getBalance() throws IOException;

    /**
     * @api {Boolean} topUp(money) Top up money
     * @apiGroup Card
     * @apiName top up
     * @apiDescription This function is used to increase the balance and write log into the card based on CNS's card specification.
     * @apiparam {Integer} money The amount of top up money.
     * @apiparam {String} datetime Transaction datetime follow this format: YYYYMMDDhhmmss.
     * @apiparam {String} samID The merchant ID, which will be written inside the log of the card.
     * @apiExample Example usage:
     *      CnsSmartCard.topUp(50000);
     * @apiSuccess (Return) {Boolean} Result True/False
     * @return
     * @throws IOException
     */
    boolean topUp(int money) throws IOException;

    /**
     * @api {Boolean} pay(money) Pay
     * @apiVersion 0.0.0
     * @apiGroup Card
     * @apiName pay
     * @apiDescription This function is used to decrease the balance and write log into the card based on CNS's card specification.
     * @apiparam {Integer} money The amount of debit money.
     * @apiparam {String} datetime Transaction datetime follow this format: YYYYMMDDhhmmss.
     * @apiparam {String} samID The merchant ID, which will be written inside the log of the card.
     * @apiExample Example usage:
     *      CnsSmartCard.pay(50000);
     * @apiSuccess (Return) {Boolean} Result True/False
     * @return
     * @throws IOException
     */
    int pay(int money) throws IOException;

    /**
     * @api {String} getFullNameVN() Get full name in Vietnamese
     * @apiGroup Card
     * @apiName getFullNameVN
     * @apiDescription This function is used to retrieve full name of the card holder in Vietnamese
     * @apiExample Example usage:
     *      String name = CnsSmartCard.getFullNameVN();
     *      Log.d(TAG,name);
     * @return
     * @throws IOException
     */
    String getFullNameVN() throws IOException;

    /**
     * @api {String} getSamID() Get SAM ID
     * @apiGroup SAM
     * @apiName getSamID
     * @apiDescription This function is used to retrieve the SAM ID, which is merchant ID
     * @apiExample {java} Example usage:
     *      String samID = CnsSmartCard.getSamID();
     *      Log.d(TAG,samID);
     * @apiSuccessExample Success-response:
     *      TAG: 0416391A682A80
     * @return
     * @throws IOException
     */
    String getSamID() throws IOException;

    boolean pocketInit() throws IOException;

    String readData(String fileID, String offSet, int lengthInt) throws IOException;

    String test() throws IOException;

//    void cardInfoInit(CardData cardData) throws IOException;

//    void cardConfigInit(CardData cardData) throws IOException;

    void cardDataInit() throws IOException;

    void topUp(String money) throws IOException;

    void encryptPIN(String DKNo, String keyVer, String PIN) throws IOException;

    void verifyTransactionLog() throws IOException;

    void testBooking() throws IOException;


}
