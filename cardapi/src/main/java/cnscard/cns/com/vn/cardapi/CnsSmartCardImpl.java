package cnscard.cns.com.vn.cardapi;

import android.util.Log;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cnscard.cns.com.vn.cardapi.utils.ByteUtils;
import cnscard.cns.com.vn.cardapi.utils.Hex;

import static cnscard.cns.com.vn.cardapi.utils.HashConstants.*;


public class CnsSmartCardImpl implements ICnsSmartCard {

    /** Class Pocket Log **/
    public class PocketLog {
        public String transactionCode, transactionDateTime;
        public int transactionCounter;
        public int beforeBalance, usedAmount, afterBalance;
        public String samID, signPocket;

        public String rawLog;
        public PocketLog(String pocketLog) {
            this.rawLog = pocketLog;
            this.transactionCode = pocketLog.substring(0,1*2);
            this.transactionCounter = ByteUtils.hexString2Int(pocketLog.substring(1*2,5*2));
            this.transactionDateTime = pocketLog.substring(5*2,12*2);
            this.beforeBalance = ByteUtils.hexString2Int(pocketLog.substring(12*2,16*2));
            this.usedAmount = ByteUtils.hexString2Int(pocketLog.substring(16*2,20*2));
            this.afterBalance = ByteUtils.hexString2Int(pocketLog.substring(20*2,24*2));
            this.samID = pocketLog.substring(24*2,31*2);
            this.signPocket = pocketLog.substring(31*2,39*2);
        }

    }

    /** Debug configuration **/
    private static boolean DebuggingEnable = true;

    private static void LogDebug (String tag, String message) {
        if (DebuggingEnable) {
            Log.d(tag, message);
        }
    }

    private String cardProduct = "";
    private String cardVersion = "";
    private String cardNumber  = "";
    private String diversifyInput = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";

    private final ICardReader nfcReader;
    private final ICardReader samReader;
    private final PocketData pocketData = new PocketData();
    private final CardData cardData = new CardData();
    CnsSmartCardImpl(ICardReader nfcReader, ICardReader samReader) throws IOException {
        this.nfcReader = nfcReader;
        this.samReader = samReader;

        nfcReader.connect();

        samReader.connect();
    }

    /************* Override CNS smart card interface ***********/

    @Override
    public String readData(String fileID, String offSet, int lengthInt) throws IOException {
        int i = 0; // Counter for receiving long data
        String data = "";
        String length = Hex.encode(ByteUtils.int2bytearray(lengthInt,3));
        String apduReadCmd = "90AD" + "0000" + "07" + fileID + offSet + length + "00";
        String response = send2Card(apduReadCmd);

        for (i = 0; i < 10; i++) {
            if (response.endsWith("9100")) {
                data += response.substring(0,response.length()-4);
                break;
            } else if (response.endsWith("91AF")) {
                data += response.substring(0,response.length()-4);
                response = send2Card("90AF");
            } else {
                throw new CnsCardException("Error: " + response);
            }
        }
        return data;
    }

    private String getValue_s(String fileID) throws IOException {
        String s = send2Card("906C" + "0000" + "01" + fileID + "00");
        if (!s.endsWith("9100")) {
            throw new CnsCardException(s);
        }
        return s.substring(0,4*2);
    }

    private int getValue_i(String fileID) throws IOException {
        String s = send2Card("906C" + "0000" + "01" + fileID + "00");
        if (!s.endsWith("9100")) {
            throw new CnsCardException(s);
        }
        int intValue = 0;
        byte[] buffer = Hex.decode(s.substring(0,s.length()-4));
        for (int i = 0; i < 4; i++) {
            intValue += (buffer[i] & 0xff) << (8 * i);
        }
        return intValue;
    }
    @Override
    public String test() throws IOException {
//        if (!samReader.isConnected()) {
//            samReader.connect();
//        }
//        cardInfoInit();
        selectApplication("0300DF");
        authenDivInp("23","03",diversifyInput);
        writeRecord_full("03","000000","0000000010000001201903200630561000000000222222");
        commitCard();


//        cardInit();

//        LogDebug("Test","Card product: " + cardProduct + " " + cardData.getCardProduct());
//        LogDebug("Test","Card version: " + cardVersion + " " + cardData.getCardVersion());
//        LogDebug("Test","Card number: "  + cardNumber  + " " + cardData.getCardNumber());
////        LogDebug("Test","Diversify input: "  + diversifyInput);
//
//////        LogDebug("Test","Card group ID: "   + cardData.getCardGroupID());
////        LogDebug("Test","Is Perso: "        + cardData.isPerso());
//////        LogDebug("Test","Chip type: "       + cardData.getChipType());
////        LogDebug("Test","Card issue date: " + cardData.getCardIssueDate());
////        LogDebug("Test","Card expired date: "  + cardData.getCardExDate());
////
//        LogDebug("Test","Pocket account number: " + pocketData.getPocketAccNum());
//        LogDebug("Test","Pocket type: " + pocketData.getPocketType());
//        LogDebug("Test","Pocket effective date: " + pocketData.getPocketEffectiveDate());
//        LogDebug("Test","Pocket expired date: " + pocketData.getPocketExpireDate());
//        LogDebug("Test","Pocket status: " + pocketData.getPocketStatus());
//        LogDebug("Test","Pocket max balance: " + pocketData.getMaxBalance());
//        LogDebug("Test","Pocket min balance: " + pocketData.getMinBalance());
//
//        LogDebug("Test","Pocket balance: " + pocketData.getPocketBalance());
        return "";
    }

    @Override
    public void verifyTransactionLog() throws IOException {
        selectApplication(DFpurse.get(cardVersion));
        List<PocketLog> transactionLog =  pocketLogInit();
        // Check signature
        for (int i = 0; i < transactionLog.size(); i++) {
            LogDebug("CNS", "Log ------------- " + i);
            LogDebug("CNS", "Transaction code: " + transactionLog.get(i).transactionCode);
            LogDebug("CNS", "Transaction counter: " + transactionLog.get(i).transactionCounter);
            LogDebug("CNS", "Transaction datetime: " + transactionLog.get(i).transactionDateTime);
            LogDebug("CNS", "Before balance: " + transactionLog.get(i).beforeBalance);
            LogDebug("CNS", "Used balance: " + transactionLog.get(i).usedAmount);
            LogDebug("CNS", "After balance: " + transactionLog.get(i).afterBalance);
            LogDebug("CNS", "SAM ID: " + transactionLog.get(i).samID);
            LogDebug("CNS", "Signature: " + transactionLog.get(i).signPocket);
            // Get log basic info
            String s = transactionLog.get(i).rawLog.substring(0,31*2);
            // Generate signature again
            s = generateSignature(DKsign_pocket.get(cardVersion),"01",s).substring(16*2,24*2);
            LogDebug("CNS","Sign 1: " + transactionLog.get(i).signPocket);
            LogDebug("CNS","Sign 2: " + s);
            if (transactionLog.get(i).signPocket.equals(s)) {
                LogDebug("CNS","Transaction is OK!!!!!!!!!!!!!");
            } else LogDebug("CNS","Fraudulent transaction");
        }
//        bookingListInit();
    }

    public void testBooking() throws IOException {
        selectApplication(DFbooking.get(cardVersion));
        authenDivInp(MKbooking.get(cardVersion),DKbooking.get(cardVersion),diversifyInput);
        Booking booking = new Booking("018707232537E04B3B2AC17478F4DAB705201808232014344444444444444444");
        updateBookingLog(booking,0);
        commitCard();
    }

    /**
     * Initialize Card
     */
    @Override
    public void cardInit() throws IOException {
        cardDataInit();
        if (cardData.isPurseEnable()) {
            pocketInfoInit();
            pocketBalanceInit();
            pocket_logInit();
        }
    }
    /************** Card Data **************/
    private void cardInfoInit() throws IOException {
//        selectApplication("0100DF");
        /** Card Info ID: 01, length: 19 bytes **/
        String s = readData("01","000000",0);
        cardData.cardInfoInit(s);

        LogDebug("CNS","card product: " + cardData.getCardProduct());
        LogDebug("CNS","card version: " + cardData.getCardVersion());
        LogDebug("CNS","card number: " + cardData.getCardNumber());

    }

    private void cardConfigInit() throws IOException {
        /** Card Config ID: 02, length: 5 bytes **/
        String s = readData("02","000000",5);
        cardData.cardConfigInit(s);

        LogDebug("CNS","is Active: " + cardData.isActive());
        LogDebug("CNS","is Active: " + cardData.isPurseEnable());
        LogDebug("CNS","is Active: " + cardData.isTopupEnable());
    }

    private void cardStatusInit() throws IOException {
        /** Card Status ID: 03, length: 9 bytes **/
        String s = readData("03","000000",9);
        cardData.cardStatusInit(s);
    }

    @Override
    public void cardDataInit() throws IOException {
        selectApplication("0100DF");
        cardInfoInit();
        cardConfigInit();
        // Optional
        //cardStatusInit(cardData);
        LogDebug("CNS","End card data init");
    }

    /************** Pocket Data ************/
    @Override
    public int getBalance() {
        return pocketData.getPocketBalance();
    }

    @Override
    public boolean pocketInit() throws IOException {
        pocketInfoInit();
        pocketBalanceInit();
        pocket_logInit();
        return true;
    }

    private void pocketInfoInit() throws IOException {
        selectApplication(DFpurse.get(cardData.getCardVersion()));
        /** Pocket Info: 01, length: 26 bytes **/
        String s = readData(EFpocket_info.get(cardData.getCardVersion()),"000000",26);
        pocketData.pocketInfoInit(s);
//        pocketData.setPocketAccNum(s.substring(0,8*2));
//        pocketData.setPocketType(s.substring(8*2,9*2));
//        pocketData.setPocketEffectiveDate(s.substring(9*2,13*2));
//        pocketData.setPocketExpireDate(s.substring(13*2,17*2));
//        pocketData.setPocketStatus(s.substring(17*2,18*2));
//        pocketData.setMaxBalance(ByteUtils.hexString2Int(s.substring(18*2,22*2)));
//        pocketData.setMinBalance(ByteUtils.hexString2Int(s.substring(22*2,26*2)));
    }

    private void pocketBalanceInit() throws IOException {
        // Optional
        selectApplication(DFpurse.get(cardData.getCardVersion()));
        pocketData.setPocketBalance(getValue_i(EFpocket_value.get(cardData.getCardVersion())));
    }

    private void pocket_logInit() throws IOException {
        // Optional
        selectApplication(DFpurse.get(cardData.getCardVersion()));
        String s = readRecord_plain(EFpocket_log.get(cardData.getCardVersion()),"000000",1);
        if (s.equals("")) {
            return;
        }
        pocketData.pocketLogInit(s);
//        pocketData.setTransactionCode(s.substring(0,1*2));
//        pocketData.setTransactionCounter(ByteUtils.hexString2Int(s.substring(1*2,5*2)));
//        pocketData.setTransactionDateTime(s.substring(5*2,12*2));
//        pocketData.setBeforeBalance(ByteUtils.hexString2Int(s.substring(12*2,16*2)));
//        pocketData.setUsedAmount(ByteUtils.hexString2Int(s.substring(16*2,20*2)));
//        pocketData.setAfterBalance(ByteUtils.hexString2Int(s.substring(20*2,24*2)));
//        pocketData.setSamID(s.substring(24*2,31*2));
//        pocketData.setSignPocket(s.substring(31*2,39*2));
        LogDebug("CNS","Pocket log: " + pocketData.pocketLogSerialize());
    }

    private List<PocketLog> pocketLogInit() throws IOException {
        String s = readRecord_plain(EFpocket_log.get(cardVersion),"000000",0);
        List<PocketLog> list = new ArrayList<>();
        LogDebug("CNS","Log: " + s);
        for (int i = s.length()/62; i > 0; i--) {
            LogDebug("CNS", "i =  " + i);
            LogDebug("CNS", "STRING: " + s.substring(i*62-62,i*62));
            list.add(new PocketLog(s.substring(i*62-62,i*62)));
        }
//        for (int i = 0; i<s.length()/78;i++) {
//            LogDebug("CNS", "Log ------------- " + i);
//            LogDebug("CNS", "Transaction code: " + list.get(i).transactionCode);
//            LogDebug("CNS", "Transaction counter: " + list.get(i).transactionCounter);
//            LogDebug("CNS", "Transaction datetime: " + list.get(i).transactionDateTime);
//            LogDebug("CNS", "Before balance: " + list.get(i).beforeBalance);
//            LogDebug("CNS", "Used balance: " + list.get(i).usedAmount);
//            LogDebug("CNS", "After balance: " + list.get(i).afterBalance);
//            LogDebug("CNS", "SAM ID: " + list.get(i).samID);
//            LogDebug("CNS", "Signature: " + list.get(i).signPocket);
//        }
        return list;
    }

    @Override
    public boolean topUp(int money) throws IOException {
        selectApplication(DFpurse.get(cardData.getCardVersion()));
        int p = getValue_i(EFpocket_value.get(cardData.getCardVersion()));
        Log.d("CNS","Before"+p);

        // Check dependencies
        if (!(cardData.isActive()&&cardData.isPurseEnable()&&cardData.isTopupEnable())) {
            return false;
            // throw new CnsCardException("your card is not allowed to top up")
        }
//        if ((pocketData.getPocketBalance() + money) > pocketData.getMaxBalance()) {
//            return false;
//        }
        authenDivInp(MKpocket_credit.get(cardData.getCardVersion()),DKpocket_credit.get(cardData.getCardVersion()),diversifyInput);
        credit(EFpocket_value.get(cardData.getCardVersion()),Hex.encode(ByteUtils.int2bytearray(money,4)));
        // Log process
        pocketData.setTransactionCode("02");
        pocketData.setTransactionCounter(pocketData.getTransactionCounter()+1);
        pocketData.setTransactionDateTime(getTransacTime());
        pocketData.setBeforeBalance(pocketData.getAfterBalance());
        pocketData.setUsedAmount(money);
        pocketData.setAfterBalance(pocketData.getBeforeBalance()+pocketData.getUsedAmount());
//        pocketData.setSamID(samID);
        pocketData.setSamID(getSamID());

//        String s = pocketData.pocketLogSerialize();
//        // Generate signature base on record
//        s = generateSignature(DKsign_pocket.get(cardVersion),"01",s.substring(0,s.length()-16));
//        pocketData.setSignPocket(s.substring(16*2,24*2));
        // Get Log
        String s = pocketData.pocketLogSerialize();
        LogDebug("CNS", "Transaction log: " + s);
        writeRecord_full(EFpocket_log.get(cardData.getCardVersion()),"000000",s);

        commitCard();
        p = getValue_i(EFpocket_value.get(cardData.getCardVersion()));
        Log.d("CNS","After: "+p);
        return true;
    }

    @Override
    public void topUp(String money) throws IOException {
        selectApplication(DFpurse.get(cardVersion));
        int p = getValue_i(EFpocket_value.get(cardVersion));
        Log.d("CNS","Before: " + p);
        authenDivInp(MKpocket_credit.get(cardVersion),DKpocket_credit.get(cardVersion),diversifyInput);
        credit(EFpocket_value.get(cardVersion),Hex.encode(ByteUtils.int2bytearray(Integer.valueOf(money),4)));
        commitCard();
        p = getValue_i(EFpocket_value.get(cardVersion));
        Log.d("CNS","After: " + p);

    }

    @Override
    public int pay(int money) throws IOException {
        selectApplication(DFpurse.get(cardData.getCardVersion()));
        int p = getValue_i(EFpocket_value.get(cardData.getCardVersion()));
        Log.d("CNS","Before: "+p);

//        // Check dependencies
//        if (!(cardData.isActive()&&cardData.isPurseEnable())) {
//            return 0;
//            // throw new CnsCardException("your card is not allowed to top up")
//        }
//        if ((pocketData.getPocketBalance() - money) < pocketData.getMinBalance()) {
//            return 0;
//        }
        authenDivInp(MKpocket_credit.get(cardData.getCardVersion()),DKpocket_credit.get(cardData.getCardVersion()),diversifyInput);
        debit(EFpocket_value.get(cardData.getCardVersion()),Hex.encode(ByteUtils.int2bytearray(money,4)));
        // Log process
        pocketData.setTransactionCode("01");
        pocketData.setTransactionCounter(pocketData.getTransactionCounter()+1);
        pocketData.setTransactionDateTime(getTransacTime());
        pocketData.setBeforeBalance(pocketData.getAfterBalance());
        pocketData.setUsedAmount(money);
        pocketData.setAfterBalance(pocketData.getBeforeBalance()-pocketData.getUsedAmount());
//        pocketData.setSamID(samID);
        pocketData.setSamID(getSamID());

        String s = pocketData.pocketLogSerialize();
//        // Generate signature base on record
//        s = generateSignature(DKsign_pocket.get(cardVersion),"01",s.substring(0,s.length()-16));
//        pocketData.setSignPocket(s.substring(16*2,24*2));
        // Get Log
//        s = pocketData.pocketLogSerialize();
        LogDebug("CNS", "Transaction log: " + s);
        writeRecord_full(EFpocket_log.get(cardData.getCardVersion()),"000000",s);

        commitCard();
        p = getValue_i(EFpocket_value.get(cardData.getCardVersion()));
        Log.d("CNS","After: "+p);
        return pocketData.getTransactionCounter();
    }

    private String generateSignature(String DKNo, String keyVer, String Log) throws IOException {
        // Active the OfflineCrypto key
        if (!samReader.isConnected()) {
            samReader.connect();
        }
        String s = send2Sam("8101" + "0000" + "02" + DKNo + keyVer);
        if (!s.endsWith("9000")) throw new CnsCardException(s);
        // Load Init Vector
        s = send2Sam("8171" + "0000" + "10" + "00000000000000000000000000000000");
        if (!s.endsWith("9000")) throw new CnsCardException(s);
        // Encipher offline data
        s = send2Sam("810E" + "0000" + "20" + Log + "00" + "00");
        if (!s.endsWith("9000")) throw new CnsCardException(s);

        return s.substring(0,s.length()-4);
    }

    public void encryptPIN(String DKNo, String keyVer, String PIN) throws IOException {
        LogDebug("NCT","PIN: " + PIN);
        // Active the OfflineCrypto key
        if (!samReader.isConnected()) {
            samReader.connect();
        }
        String s = send2Sam("8101" + "0000" + "02" + DKNo + keyVer);
        if (!s.endsWith("9000")) throw new CnsCardException(s);
        // Load Init Vector
        s = send2Sam("8171" + "0000" + "10" + "00000000000000000000000000000000");
        if (!s.endsWith("9000")) throw new CnsCardException(s);
        // Encipher offline data
        s = send2Sam("810E" + "0000" + "10" + PIN + "00000000000000000000000000" + "00");
        if (!s.endsWith("9000")) throw new CnsCardException(s);

        LogDebug("NCT","PIN: " + s.substring(0,s.length()-4));
//        return s.substring(0,s.length()-4);
    }
    /****************** Booking ******************/

    public List<Booking> bookingListInit() throws IOException {
        selectApplication(DFbooking.get(cardVersion));
        String s = readRecord_plain(EFbooking.get(cardVersion),"000000",0);
        List<Booking> list = new ArrayList<>();
        for (int i = s.length()/64; i > 0; i--) {
//            LogDebug("CNS", "i =  " + i);
            LogDebug("CNS", "STRING: " + s.substring(i*64-64,i*64));
            list.add(new Booking(s.substring(i*64-64,i*64)));
        }
        for (int i = 0; i<list.size()/*s.length()/64*/;i++) {
            LogDebug("CNS", "Log ------------- " + i);
            LogDebug("CNS", "Order status: " + list.get(i).getOrderStatus());
            LogDebug("CNS", "Order number: " + list.get(i).getOrderNumber());
            LogDebug("CNS", "Product ID 1: " + list.get(i).getProductID1());
            LogDebug("CNS", "Product quantity 1: " + list.get(i).getProducQuan1());
            LogDebug("CNS", "Product ID 2: " + list.get(i).getProductID2());
            LogDebug("CNS", "Product quantity 2: " + list.get(i).getProducQuan2());
            LogDebug("CNS", "Product ID 3: " + list.get(i).getProductID3());
            LogDebug("CNS", "Product quantity 3: " + list.get(i).getProducQuan3());
            LogDebug("CNS", "Product ID 4: " + list.get(i).getProductID4());
            LogDebug("CNS", "Product quantity 4: " + list.get(i).getProducQuan4());
            LogDebug("CNS", "Booking device ID: " + list.get(i).getBookingDeviceID());
            LogDebug("CNS", "Delivery device ID: " + list.get(i).getDeliveryDeviceID());
            LogDebug("CNS", "Transaction counter: " + list.get(i).getTransacCounter());
            LogDebug("CNS", "Order datetime: " + list.get(i).getOrderDatetime());
        }
        return list;
    }

    public boolean updateBookingLog (Booking booking, int recNo) throws IOException {
        String s = booking.bookingSerialize();
        updateRecord_full(EFbooking.get(cardData.getCardVersion()),recNo,"000000",s);
        return true;
    }


    /****************** Holder Data **************/

    @Override
    public String getFullNameVN() throws IOException {
        selectApplication(DFid.get(cardVersion));
        authenDivInp(MKread_card_holder.get(cardVersion),DKread_card_holder.get(cardVersion),diversifyInput);
        String s = readData(EFcard_holder_secure.get(cardVersion),"640000",0);
        return s.substring(0,60*2);
    }

    /******************** SAM & Card communicating function ********************/
    /**
     * Select application
     * @param AID
     * @throws IOException
     */
    private void selectApplication(String AID) throws IOException {
        String response = send2Card("905A" + "0000" + "03" + AID + "00");
        if (!response.endsWith("9100")) {
            throw new CnsCardException(response);
        }
    }

    /**
     * Send string command to card
     * @param command
     * @return String
     * @throws IOException
     */
    private String send2Card(String command) throws IOException {
        LogDebug("CNS"," -> Card: " + command);
        byte[] bytes = send2Card(Hex.decode(command));
        LogDebug("CNS"," <- Card: " + Hex.encode(bytes));
        return Hex.encode(bytes);
    }

    private byte[] send2Card(byte[] command) throws IOException {
        return nfcReader.transceive(command);
    }

    private boolean commitCard() throws IOException {
        String s = send2Card("90C7000000");
        if (!s.endsWith("9100")) {
            throw new CnsCardException(s);
        }
        return true;
    }

    private String send2Sam(String command) throws IOException {
        LogDebug("CNS"," -> SAM: " + command);
        byte[] bytes = send2Sam(Hex.decode(command));
        LogDebug("CNS"," <- SAM: " +  Hex.encode(bytes));
        return Hex.encode(bytes);
    }

    private byte[] send2Sam(byte[] command) throws IOException {
        return samReader.transceive(command);
    }

    private boolean debit(String fileID, String data_4byte) throws IOException {
        // Create enciphered message
        String s = send2Sam("80ED0002" + "06" + "DC" + fileID + data_4byte + "00");
        if (!s.endsWith("9000")) {
            LogDebug("CNS","Get cipher failed");
            throw new CnsCardException(s);
        }
        // Filter encipher only
        s = s.substring(0,s.length()-4);
        // Send encipher command to card
        s = send2Card("90DC" + "0000" + "11" + fileID + s + "00");
        if (!s.endsWith("9100")) {
            throw new CnsCardException(s);
        }
        s = "00" + s.substring(0,s.length()-4);
        // Get decipher and verify MAC
        s = "805C0000" + Hex.encode(ByteUtils.int2bytearray(s.length()/2,1)) +s;
        s = send2Sam(s);
        if (!s.endsWith("9000")) {
            LogDebug("CNS", "Debit failed");
            throw new CnsCardException(s);
        }
        LogDebug("CNS","Debit succeed");
        return true;
    }

    private boolean credit(String fileID, String data_4byte) throws IOException {
        // Create enciphered message
        String s = send2Sam("80ED0002" + "06" + "0C" + fileID + data_4byte + "00");
        if (!s.endsWith("9000")) {
            LogDebug("CNS","Get cipher failed");
            throw new CnsCardException(s);
        }
        // Filter encipher only
        s = s.substring(0,s.length()-4);
        // Send encipher command to card
        s = send2Card("900C" + "0000" + "11" + fileID + s + "00");
        if (!s.endsWith("9100")) {
            throw new CnsCardException(s);
        }
        s = "00" + s.substring(0,s.length()-4);
        // Get decipher and verify MAC
        s = "805C0000" + Hex.encode(ByteUtils.int2bytearray(s.length()/2,1)) +s;
        s = send2Sam(s);
        if (!s.endsWith("9000")) {
            LogDebug("CNS", "Credit failed");
            throw new CnsCardException(s);
        }
        LogDebug("CNS","Credit succeed");
        return true;
    }

    @Override
    public String getSamID() throws IOException {
        String s = send2Sam("8060000000");
        if (s.endsWith("9000")) {
            LogDebug("CNS","card number: " + cardNumber);
            return s.substring(14*2,21*2);
        } else {
            throw new CnsCardException(s);
        }
    }

    /**
     * SAM authenticates with card
     * @param keyNoSAM - MK number in SAM
     * @param keyNoCard - DK number in Card
     * @param divInp - Diversify input
     * @return
     * @throws IOException
     */
    private boolean authenDivInp(String keyNoSAM, String keyNoCard, String divInp) throws IOException {
        boolean authenResult = false;

        String s = send2Card("90AA" + "0000" + "01" + keyNoCard + "00");
        if (s.endsWith("91AF")) {
            s = s.substring(0,s.length()-4);
            LogDebug("CNS", "E(Kx,RndB): " + s);
        }
        //                                                       Keyver
        s = send2Sam("800A" + "0100" + "22" + keyNoSAM + "00" + s + divInp + "00");
        s = s.substring(0, s.length() - 4);
        s = send2Card("90AF0000" + "20" + s + "00");
        switch (s) {
            case "91AF":
                LogDebug("CNS", "RndB was wrong");
                break;
            case "917F":
                LogDebug("CNS", "Length error");
                throw new CnsCardException("Error: " + s);
            default:
                s = s.substring(0,s.length()-4);
                s = send2Sam("800A" + "0000" + "10" + s);
                switch (s) {
                    case "9000":
                        LogDebug("CNS", "Authenticate succeed");
                        authenResult = true;
                        break;
                    case "6700":
                        LogDebug("CNS", "Wrong Length");
                        throw new CnsCardException("Error: " + s);
                    case "6A80":
                        LogDebug("CNS", "");
                    default:
                        break;
                }
                break;
        }
        return authenResult;
    }

    private String readRecord_plain (String fileID, String offSetRecNum, int recordQuantity) throws IOException {
        int i = 0;
        String logData = "";
        String length = Hex.encode(ByteUtils.int2bytearray(recordQuantity,3));
        // Create apdu cmd
        String s = "90BB" + "0000" + "07" + fileID + offSetRecNum + length + "00";
        s = send2Card(s);

        for (i = 0; i < 10; i++) {
            if (s.endsWith("9100")) {
                LogDebug("CNS","Read all data");
                logData += s.substring(0,s.length()-4);
                break;
            } else if (s.endsWith("91AF")) {
                LogDebug("CNS","read data continue");
                logData += s.substring(0,s.length()-4);
                s = send2Card("90AF000000");
            } else if (s.endsWith("91BE")) {
                return "";
            } else {
                throw new CnsCardException(s);
            }
        }
        return logData;
    }

    private boolean writeRecord_full (String fileID, String offSet, String data) throws IOException {
        String cardAPDU = "8B" + fileID + offSet + Hex.encode(ByteUtils.int2bytearray(data.length()/2,3)) + data;
        // Prepare encrypted data
        String s = send2Sam("80ED" + "0008" +
                Hex.encode(ByteUtils.int2bytearray(cardAPDU.length()/2,1)) + cardAPDU + "00");
        if (s.endsWith("9000")) {
            LogDebug("CNS","Encipher succeed");
            s = s.substring(0,s.length()-4);
        } else {
            throw new CnsCardException(s);
        }
        // Send encrypted data                   Length
        s = send2Card("908B" + "0000" +
                Hex.encode(ByteUtils.int2bytearray(1+3+3 + s.length()/2,1)) + fileID + offSet +
                Hex.encode(ByteUtils.int2bytearray(data.length()/2,3)) + s + "00");
        if (!s.endsWith("9100")) {
            throw new CnsCardException(s);
        }
        return true;
    }

    private boolean updateRecord_full (String fileID, int recNo, String offSet, String data) throws IOException {
        String cardAPDU = "BA" + fileID + Hex.encode(ByteUtils.int2bytearray(recNo,3)) + offSet +
                Hex.encode(ByteUtils.int2bytearray(data.length()/2,3)) +data;
        // Prepare encrypted data
        String s = send2Sam("80ED" + "000B" +
                Hex.encode(ByteUtils.int2bytearray(cardAPDU.length()/2,1)) + cardAPDU + "00");
        if (s.endsWith("9000")) {
            LogDebug("CNS","Encipher succeed");
            s = s.substring(0,s.length()-4);
        } else {
            throw new CnsCardException(s);
        }
        // Send encrypted data                   Length
        s = send2Card("90BA" + "0000" +
                Hex.encode(ByteUtils.int2bytearray(1+3+3+3 + s.length()/2,1)) + fileID +
                Hex.encode(ByteUtils.int2bytearray(recNo,3)) + offSet +
                Hex.encode(ByteUtils.int2bytearray(data.length()/2,3)) + s + "00");
        if (!s.endsWith("9100")) {
            throw new CnsCardException(s);
        }
        return true;
    }

    /***************************** System process ****************************/
    String getTransacTime () {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date currentTime = Calendar.getInstance().getTime();
        String simpleTime = sdf.format(currentTime);
        return simpleTime;
    }
}
