package cnscard.cns.com.vn.cardapi;

import java.io.IOException;

public interface ICardReader {
    /**
     * Enable I/O operations to the reader
     */
    void connect() throws IOException;

    /**
     * Disable I/O operations to the reader
     */
    void close() throws IOException;

    /**
     *  Helper to indicate if I/O operations should be possible.
     * @return true if connected
     */
    boolean isConnected();

    /**
     * Send raw data to the reader and receive the response.
     * @param data APDU Command data
     * @return APDU Response data
     */
    byte[] transceive(byte[] data) throws IOException;
}
