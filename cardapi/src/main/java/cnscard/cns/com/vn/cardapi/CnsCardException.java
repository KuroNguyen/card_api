package cnscard.cns.com.vn.cardapi;

public class CnsCardException extends RuntimeException {
    public CnsCardException(){
    }
    public CnsCardException(String message){
        super(message);
    }
    public CnsCardException(String message, Throwable cause){
        super(message, cause);
    }
}

