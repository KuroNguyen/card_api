define({
  "name": "CNS SAM & CARD API",
  "version": "0.0.1",
  "description": "SAM & CARD API for Android device base on the specification of CNS",
  "title": "CNS API",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-10-31T02:38:25.298Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
