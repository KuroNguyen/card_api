package cnscard.cns.com.vn.cardapi.utils;

import java.util.HashMap;
import java.util.Map;

public final class HashConstants {
    private HashConstants() {

    }
    /** DF Hash **/
    public static final Map<String, String> DFcard = new HashMap<>();
    public static final Map<String, String> DFpurse = new HashMap<>();
    public static final Map<String, String> DFid = new HashMap<>();
    public static final Map<String, String> DFbooking = new HashMap<>();

    /** EF Hash **/
    public static final Map<String, String> EFcard_info = new HashMap<>();
    public static final Map<String, String> EFcard_config = new HashMap<>();
    public static final Map<String, String> EFcard_status = new HashMap<>();

    public static final Map<String, String> EFpocket_info = new HashMap<>();
    public static final Map<String, String> EFpocket_value = new HashMap<>();
    public static final Map<String, String> EFpocket_log = new HashMap<>();
    public static final Map<String, String> EFpocket_topup_log = new HashMap<>();
    public static final Map<String, String> EFloyalty_log = new HashMap<>();
    public static final Map<String, String> EFsaving_log = new HashMap<>();

    public static final Map<String, String> EFcard_holder_common = new HashMap<>();
    public static final Map<String, String> EFcard_holder_secure = new HashMap<>();
    public static final Map<String, String> EFaccess_att_log = new HashMap<>();
    public static final Map<String, String> EFaccess_lib_log = new HashMap<>();
    public static final Map<String, String> EFaccess_park_log = new HashMap<>();

    public static final Map<String, String> EFbooking = new HashMap<>();

    /** SAM keys Hash **/
    public static final Map<String, String> DKsam_master = new HashMap<>();
    public static final Map<String, String> MKvalidate_ticket = new HashMap<>();
    public static final Map<String, String> DKsign_ticket = new HashMap<>();
    public static final Map<String, String> DKsign_pocket = new HashMap<>();
    public static final Map<String, String> MKcard_config = new HashMap<>();
    public static final Map<String, String> MKcard_status = new HashMap<>();
    public static final Map<String, String> MKticket_new = new HashMap<>();
    public static final Map<String, String> MKticket_use = new HashMap<>();
    public static final Map<String, String> MKticket_topup = new HashMap<>();
    public static final Map<String, String> MKpocket_info = new HashMap<>();
    public static final Map<String, String> MKpocket_debit = new HashMap<>();
    public static final Map<String, String> MKpocket_cancel = new HashMap<>();
    public static final Map<String, String> MKpocket_credit = new HashMap<>();
    public static final Map<String, String> MKloyalty = new HashMap<>();
    public static final Map<String, String> MKsaving = new HashMap<>();
    public static final Map<String, String> MKpurse_tmri = new HashMap<>();
    public static final Map<String, String> MKcard_holder = new HashMap<>();
    public static final Map<String, String> MKread_card_holder = new HashMap<>();
    public static final Map<String, String> MKattendance = new HashMap<>();
    public static final Map<String, String> MKlibrary = new HashMap<>();
    public static final Map<String, String> MKparking = new HashMap<>();
    public static final Map<String, String> MKbooking = new HashMap<>();
    public static final Map<String, String> DKunlock = new HashMap<>();

    /** Card keys Hash **/
    public static final Map<String, String> DKcard_config = new HashMap<>();
    public static final Map<String, String> DKcard_status = new HashMap<>();
    public static final Map<String, String> DKpocket_info = new HashMap<>();
    public static final Map<String, String> DKpocket_debit = new HashMap<>();
    public static final Map<String, String> DKpocket_cancel = new HashMap<>();
    public static final Map<String, String> DKpocket_credit = new HashMap<>();
    public static final Map<String, String> DKloyalty = new HashMap<>();
    public static final Map<String, String> DKsaving = new HashMap<>();
    public static final Map<String, String> DKcard_holder = new HashMap<>();
    public static final Map<String, String> DKread_card_holder = new HashMap<>();
    public static final Map<String, String> DKattendance = new HashMap<>();
    public static final Map<String, String> DKlibrary = new HashMap<>();
    public static final Map<String, String> DKparking = new HashMap<>();
    public static final Map<String, String> DKbooking = new HashMap<>();

        static {
            /** Application ID **/
            DFcard.put("01","0100DF");
            DFcard.put("02","1AFC01");

            DFpurse.put("01","0200DF");
            DFpurse.put("02","1AFC03");

            DFid.put("01","0300DF");
            DFid.put("02","1AFC04");

            DFbooking.put("01","0400DF");
            DFbooking.put("02","1AFC05");

            /** File ID **/
            // DFcard
            EFcard_info.put("01","01");
            EFcard_info.put("02","01");

            EFcard_config.put("01","02");
            EFcard_config.put("02","02");

            EFcard_status.put("01","03");
            EFcard_status.put("02","03");

            // DFpurse
            EFpocket_info.put("01","01");
            EFpocket_info.put("02","01");

            EFpocket_value.put("01","02");
            EFpocket_value.put("02","02");

            EFpocket_log.put("01","03");
            EFpocket_log.put("02","03");

            EFpocket_topup_log.put("01","04");
            EFpocket_topup_log.put("02","04");

            EFloyalty_log.put("02","05");
            EFsaving_log.put("02","06");

            // DFid
            EFcard_holder_common.put("01","01");
            EFcard_holder_common.put("02","01");

            EFcard_holder_secure.put("01","02");
            EFcard_holder_secure.put("02","02");

            EFaccess_att_log.put("01","03");
            EFaccess_att_log.put("02","03");

            EFaccess_lib_log.put("02","04");

            EFaccess_park_log.put("01","04");
            EFaccess_park_log.put("02","05");

            // DFbooking
            EFbooking.put("01","01");
            EFbooking.put("02","01");

            /** Key in SAM **/
            // DFcard
            MKcard_config.put("01","0A");

            MKcard_status.put("01","0B");

            // DFpurse
            MKpocket_info.put("01","11");

            MKpocket_debit.put("01","12");
            MKpocket_debit.put("02","32");

            MKpocket_cancel.put("01","13");

            MKpocket_credit.put("01","14");
            MKpocket_credit.put("02","34");

            MKloyalty.put("01","15");

            MKsaving.put("01","16");

            // DFid
            MKcard_holder.put("01","21");

            MKread_card_holder.put("01","22");
            MKread_card_holder.put("02","42");

            MKattendance.put("01","23");

            MKlibrary.put("01","24");

            MKparking.put("01","25");

            // DFbooking
            MKbooking.put("01","2B");
            MKbooking.put("02","51");

            /** Key in Card **/
            // DFcard
            DKcard_config.put("01","01");

            DKcard_status.put("01","02");

            // DFpurse
            DKpocket_info.put("01","01");

            DKpocket_debit.put("01","02");

            DKpocket_cancel.put("01","03");

            DKpocket_credit.put("01","04");
            DKpocket_credit.put("02","04");

            DKloyalty.put("01","05");

            DKsaving.put("01","06");

            DKsign_pocket.put("02","0C");

            // DFid
            DKcard_holder.put("01","01");

            DKread_card_holder.put("01","02");
            DKread_card_holder.put("02","02");

            DKattendance.put("01","03");

            DKlibrary.put("01","04");

            DKparking.put("01","05");

            // DFbooking
            DKbooking.put("01","01");
            DKbooking.put("02","01");
        }
}
