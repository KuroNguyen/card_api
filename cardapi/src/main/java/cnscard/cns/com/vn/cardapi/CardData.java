package cnscard.cns.com.vn.cardapi;

import android.util.Log;

public class CardData {
    //Class status
    private boolean InfoInit = false;
    private boolean ConfigInit = false;
    private boolean StatusInit = false;


    /**
     * ****** Card info: 19 bytes ******
     * Card Product:        2 |
     * Card Version:        2 |
     * Card Number:        10 |
     * Card Group ID:       4 |
     * Recycle Card ID:     1 |
     * Card Company:        3 |
     * Card Format Amount:  4 |
     * Deposit Amount:      4 |
     * Is Perso:            1 |
     * Chip Type:           1 |
     * Card Issue Date:     4 |
     * Card Expired Date:   4 |
     * **/
    private String cardProduct, cardVersion, cardNumber, cardGroupID, cardCompany, cardFormatAmount, depositAmount, chipType, cardIssueDate, cardExDate;
    private boolean recycleCardID, isPerso;

    /**
     * ****** Card config: 16 bytes ******
     * Is Active:           1 |
     * Is Purse Enable:     1 |
     * Is Topup Enable:     1 |
     * Is Auto Load Enable: 1 |
     * Is Return Enable:    1 |
     * Is Booking Enable:   1 |
     * Is Main Device:      1 |
     * Is Calling Enable:   1 |
     * Is SMS Enable:       1 |
     * Is Printing Enable:  1 |
     * RFU1:                1 |
     * RFU2:                1 |
     * RFU3:                1 |
     * RFU4:                1 |
     * RFU5:                1 |
     * RFU6:                1 |
     * **/
    private boolean isActive, isPurseEnable, isTopupEnable, isAutoLoadEnable, isReturnEnable;
    private boolean isBookingEnable, isMainDevice, isCallingEnable, isSMSEnable, isPrintingEnable;
    private boolean RFU1, RFU2, RFU3, RFU4, RFU5, RFU6;
    // Card status: 9 bytes
    /**
     * ****** Card status: 9 bytes ******
     * Card Status:         1 |
     * Lock Date:           4 |
     * Unlock Date:         4 |
     * **/
    private String cardStatus, lockDate, unlockDate;

    public CardData() {
        this.cardProduct        = "0000";
        this.cardVersion        = "0000";
        this.cardNumber         = "00000000000000000000";
        this.cardGroupID        = "00000000";
        this.recycleCardID      = false;
        this.cardCompany        = "000000";
        this.cardFormatAmount   = "00000000";
        this.cardIssueDate      = "00000000";
        this.cardExDate         = "00000000";

        this.isActive           = false;
        this.isPurseEnable      = false;
        this.isTopupEnable      = false;
        this.isAutoLoadEnable   = false;
        this.isReturnEnable     = false;
        this.isBookingEnable    = false;
        this.isMainDevice       = false;
        this.isCallingEnable    = false;
        this.isSMSEnable        = false;
        this.isPrintingEnable   = false;
        this.RFU1               = false;
        this.RFU2               = false;
        this.RFU3               = false;
        this.RFU4               = false;
        this.RFU5               = false;
        this.RFU6               = false;

        this.cardStatus         = "03";
        this.lockDate           = "00000000";
        this.unlockDate         = "00000000";
    }

    public void cardInfoInit(String data) {
        if (data.length()== 80/*(40x2)*/) {
            // 0 -> (2x2): 4
            this.cardProduct = data.substring(0,4);
            // 4 -> (2+2)x2: 8
            this.cardVersion = data.substring(4,8);
            // 8 -> (2+2+10)x2: 28
            this.cardNumber = data.substring(8,28);
            // 28 -> (2+2+10+4)x2: 36
            this.cardGroupID = data.substring(28,36);
            // 36 -> (2+2+10+4+1)x2: 38
            if (data.substring(36,38)=="01") {
                this.recycleCardID = true;
            }
            // 38 -> (2+2+10+4+1+3)x2: 44
            this.cardCompany = data.substring(38,44);
            // 44 -> (2+2+10+4+1+3+4)x2: 52
            this.cardFormatAmount = data.substring(44,52);
            // 52 -> (2+2+10+4+1+3+4+4)x2: 60
            this.depositAmount = data.substring(52,60);
            // 60 -> (2+2+10+4+1+3+4+4+1)x2: 62
            if (data.substring(60,62)=="01") {
                this.isPerso = true;
            }
            // 62 -> (2+2+10+4+1+3+4+4+1+1)x2: 64
            this.chipType = data.substring(62,64);
            // 64 -> (2+2+10+4+1+3+4+4+1+1+4)x2: 72
            this.cardIssueDate = data.substring(64,72);
            // 72 -> (2+2+10+4+1+3+4+4+1+1+4+4)x2: 80
            this.cardExDate = data.substring(72,80);

            this.InfoInit = true;
        }
        else {
            this.InfoInit = false;
        }
    }

    public void cardConfigInit(String data) {
        if (data.length()==32 /*(16x2)*/) {
            // 0  -> 2
            if (data.substring(0,2).equals("01")) {
                this.isActive = true;
            }
            // 2  -> 4
            if (data.substring(2,4).equals("01")) {
                this.isPurseEnable = true;
            }
            // 4  -> 6
            if (data.substring(4,6).equals("01")) {
                this.isTopupEnable = true;
            }
            // 6  -> 8
            if (data.substring(6,8).equals("01")) {
                this.isAutoLoadEnable = true;
            }
            // 8  -> 10
            if (data.substring(8,10).equals("01")) {
                this.isReturnEnable = true;
            }
            // 10 -> 12
            if (data.substring(10,12).equals("01")) {
                this.isBookingEnable = true;
            }
            // 12 -> 14
            if (data.substring(12,14).equals("01")) {
                this.isMainDevice = true;
            }
            // 14 -> 16
            if (data.substring(14,16).equals("01")) {
                this.isCallingEnable = true;
            }
            // 16 -> 18
            if (data.substring(16,18).equals("01")) {
                this.isSMSEnable = true;
            }
            // 18 -> 20
            if (data.substring(18,20).equals("01")) {
                this.isPrintingEnable = true;
            }
            // 20 -> 22
            if (data.substring(20,22).equals("01")) {
                this.RFU1 = true;
            }
            // 22 -> 24
            if (data.substring(22,24).equals("01")) {
                this.RFU2 = true;
            }
            // 24 -> 26
            if (data.substring(24,26).equals("01")) {
                this.RFU3 = true;
            }
            // 26 -> 28
            if (data.substring(26,28).equals("01")) {
                this.RFU4 = true;
            }
            // 28 -> 30
            if (data.substring(28,30).equals("01")) {
                this.RFU5 = true;
            }
            // 30 -> 32
            if (data.substring(30,32).equals("01")) {
                this.RFU6 = true;
            }

            this.ConfigInit = true;
        }
        else {
            this.ConfigInit = false;
        }
    }

    public void cardStatusInit(String data) {
        if (data.length()==18 /*(9x2)*/) {
            // 0 -> (1x2): 2
            this.cardStatus = data.substring(0,2);
            // 2 -> (1+4)x2: 10
            this.lockDate = data.substring(2,10);
            // 10 -> (1+4+4)x2: 18
            this.unlockDate = data.substring(4,18);

            this.StatusInit = true;
        }
        else {
            this.StatusInit = false;
        }
    }

    public boolean isInfoInit() {
        return InfoInit;
    }

    public boolean isConfigInit() {
        return ConfigInit;
    }

    public boolean isStatusInit() {
        return StatusInit;
    }

    public String getCardProduct() {
        return cardProduct;
    }

    public String getCardVersion() {
        return cardVersion;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardIssueDate() {
        return cardIssueDate;
    }

    public String getCardExDate() {
        return cardExDate;
    }

    public boolean isPerso() {
        return isPerso;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isPurseEnable() {
        return isPurseEnable;
    }

    public boolean isTopupEnable() {
        return isTopupEnable;
    }

    public boolean isAutoLoadEnable() {
        return isAutoLoadEnable;
    }

    public boolean isBookingEnable() {
        return isBookingEnable;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public String getLockDate() {
        return lockDate;
    }

    public String getUnlockDate() {
        return unlockDate;
    }
}
