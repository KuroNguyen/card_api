package cnscard.cns.com.vn.cardapi.utils;

import android.util.Log;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class ByteUtils {
    public static <T> T[] concat(T[] first, T[]... rest) {
        int totalLength = first.length;
        for (T[] array : rest) {
            totalLength += array.length;
        }
        T[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;
        for (T[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    public static byte[] int2bytearray(int i, int index) {
        byte[] result = new byte[index];

        for (int k = 0; k < index; k++) {
            result[k] = (byte) (i >> 8*k);
        }
        return result;
    }

    public static byte[] int2Bytes(int i) {
        Log.d("CNS", Hex.encode(ByteBuffer.allocate(4).putInt(i).array()));
        return ByteBuffer.allocate(4).putInt(i).array();
    }

    public static byte[] short2Bytes(short i) {
        return ByteBuffer.allocate(2).putShort(i).array();
    }

    public static int bytes2Int(byte[] bytes, int offset, int len) {
        byte[] tmp = new byte[len];
        System.arraycopy(bytes, offset, tmp, 0, len);
        return bytes2Int(tmp);
    }

    public static int bytes2Int(byte[] bytes) {
        return new BigInteger(bytes).intValue();
    }

    public static int hexString2Int(String strValue) {
        int intValue = 0;
        byte[] buffer = Hex.decode(strValue);
        for (int i = 0; i < buffer.length; i++) {
            intValue += (buffer[i] & 0xff) << (8*i);
        }
        return intValue;
    }
}

