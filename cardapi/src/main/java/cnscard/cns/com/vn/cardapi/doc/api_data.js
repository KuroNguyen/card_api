define({ "api": [
  {
    "type": "Void",
    "url": "cardInit()",
    "title": "Initial card",
    "group": "Card",
    "name": "cardInit",
    "description": "<p>This function need to be called when the card tapped in the first time.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "CnsSmartCard.cardInit();",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "./ICnsSmartCard.java",
    "groupTitle": "Card"
  },
  {
    "type": "Integer",
    "url": "getBalance()",
    "title": "Get balance",
    "group": "Card",
    "name": "getBalance",
    "description": "<p>This function is used to get the balance of the card</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "int balance = CnsSmartCard.getBalance();",
        "type": "java"
      }
    ],
    "version": "0.0.0",
    "filename": "./ICnsSmartCard.java",
    "groupTitle": "Card"
  },
  {
    "type": "String",
    "url": "getFullNameVN()",
    "title": "Get full name in Vietnamese",
    "group": "Card",
    "name": "getFullNameVN",
    "description": "<p>This function is used to retrieve full name of the card holder in Vietnamese</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "String name = CnsSmartCard.getFullNameVN();\nLog.d(TAG,name);",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "./ICnsSmartCard.java",
    "groupTitle": "Card"
  },
  {
    "type": "Boolean",
    "url": "pay(money)",
    "title": "Pay",
    "version": "0.0.0",
    "group": "Card",
    "name": "pay",
    "description": "<p>This function is used to decrease the balance and write log into the card based on CNS's card specification.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "money",
            "description": "<p>The amount of debit money.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "datetime",
            "description": "<p>Transaction datetime follow this format: YYYYMMDDhhmmss.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "samID",
            "description": "<p>The merchant ID, which will be written inside the log of the card.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "CnsSmartCard.pay(50000);",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Return": [
          {
            "group": "Return",
            "type": "Boolean",
            "optional": false,
            "field": "Result",
            "description": "<p>True/False</p>"
          }
        ]
      }
    },
    "filename": "./ICnsSmartCard.java",
    "groupTitle": "Card"
  },
  {
    "type": "Boolean",
    "url": "topUp(money)",
    "title": "Top up money",
    "group": "Card",
    "name": "top_up",
    "description": "<p>This function is used to increase the balance and write log into the card based on CNS's card specification.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "money",
            "description": "<p>The amount of top up money.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "datetime",
            "description": "<p>Transaction datetime follow this format: YYYYMMDDhhmmss.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "samID",
            "description": "<p>The merchant ID, which will be written inside the log of the card.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "CnsSmartCard.topUp(50000);",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Return": [
          {
            "group": "Return",
            "type": "Boolean",
            "optional": false,
            "field": "Result",
            "description": "<p>True/False</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./ICnsSmartCard.java",
    "groupTitle": "Card"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "E__ProgramWorkspace_Android_workspace_testAPI_cardapi_src_main_java_cnscard_cns_com_vn_cardapi_doc_main_js",
    "groupTitle": "E__ProgramWorkspace_Android_workspace_testAPI_cardapi_src_main_java_cnscard_cns_com_vn_cardapi_doc_main_js",
    "name": ""
  },
  {
    "type": "String",
    "url": "getSamID()",
    "title": "Get SAM ID",
    "group": "SAM",
    "name": "getSamID",
    "description": "<p>This function is used to retrieve the SAM ID, which is merchant ID</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "String samID = CnsSmartCard.getSamID();\nLog.d(TAG,samID);",
        "type": "java"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-response:",
          "content": "TAG: 0416391A682A80",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./ICnsSmartCard.java",
    "groupTitle": "SAM"
  }
] });
