package cnscard.cns.com.vn.cardapi;

import android.util.Log;

import cnscard.cns.com.vn.cardapi.utils.ByteUtils;
import cnscard.cns.com.vn.cardapi.utils.Hex;

public class PocketLog {

    // Pocket Log: 31 bytes
    /** Transaction Code: 1 | Counter: 4 | Datetime: 7 | Before: 4 | Used: 4 | After: 4 | SAM ID: 7 **/
    public String transactionCode, transactionDateTime;
    public int transactionCounter;
    public int beforeBalance, usedAmount, afterBalance;
    public String samID;

    public String rawLog;
    public boolean pocketLogInit(String pocketLog) {
        if(pocketLog.length() == 62 /*(31x2)*/) {
//            this.rawLog = pocketLog;
            // 0 -> 1x2: 2
            this.transactionCode = pocketLog.substring(0,1*2);
            // 2 -> (1+4)x2: 10
            this.transactionCounter = ByteUtils.hexString2Int(pocketLog.substring(2,10));
            // 10 -> (1+4+7): 24
            this.transactionDateTime = pocketLog.substring(10,24);
            // 24 -> (1+4+7+4)x2: 32
            this.beforeBalance = ByteUtils.hexString2Int(pocketLog.substring(24,32));
            // 32 -> (1+4+7+4+4)x2: 40
            this.usedAmount = ByteUtils.hexString2Int(pocketLog.substring(32,40));
            // 40 -> (1+4+7+4+4+4)x2: 48
            this.afterBalance = ByteUtils.hexString2Int(pocketLog.substring(40,48));
            // 48 -> (1+4+7+4+4+4+7)x2: 62
            this.samID = pocketLog.substring(48,62);

            return true;
        } else {

            return false;
        }
    }

    public PocketLog() {
        this.transactionCode="";
        this.transactionDateTime="";
        this.samID="";
    }

    public String pocketLogSerialize() {
        Log.d("CNS","AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA: " + Hex.encode(ByteUtils.int2bytearray(getTransactionCounter(),4)));
        String s = getTransactionCode() + Hex.encode(ByteUtils.int2bytearray(getTransactionCounter(),4)) +
                getTransactionDateTime() + Hex.encode(ByteUtils.int2bytearray(getBeforeBalance(),4)) +
                Hex.encode(ByteUtils.int2bytearray(getUsedAmount(),4)) +
                Hex.encode(ByteUtils.int2bytearray(getAfterBalance(),4)) +
                getSamID();
        return s;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public int getTransactionCounter() {
        return transactionCounter;
    }

    public int getBeforeBalance() {
        return beforeBalance;
    }

    public int getUsedAmount() {
        return usedAmount;
    }

    public int getAfterBalance() {
        return afterBalance;
    }

    public String getSamID() {
        return samID;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public void setTransactionCounter(int transactionCounter) {
        this.transactionCounter = transactionCounter;
    }

    public void setBeforeBalance(int beforeBalance) {
        this.beforeBalance = beforeBalance;
    }

    public void setUsedAmount(int usedAmount) {
        this.usedAmount = usedAmount;
    }

    public void setAfterBalance(int afterBalance) {
        this.afterBalance = afterBalance;
    }

    public void setSamID(String samID) {
        this.samID = samID;
    }
}
