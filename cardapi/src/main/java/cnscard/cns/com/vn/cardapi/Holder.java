package cnscard.cns.com.vn.cardapi;

public class Holder {
    // Card holder common
    private String cardHolderID, docType, docNum, docExpDate, customerType;
    // Card holder secure
    private String fullName, gender, dob, orgCode, orgName, pob, fullNameVN;
    // Access att log
    private String att_deviceID, att_datetime, att_location;
    // Access lib log
    private String lib_deviceID, lib_datetime, lib_loanRecordID, lib_loanStatus;
    // Access park log
    private String park_lotID, park_deviceID, park_datetime, park_type, park_mode;

    public Holder() {
        this.cardHolderID = "";
        this.docType = "";
        this.docNum = "";
        this.docExpDate = "";
        this.customerType = "";
        this.fullName = "";
        this.gender = "";
        this.dob = "";
        this.orgCode = "";
        this.orgName = "";
        this.fullNameVN = "";
        this.att_deviceID = "";
        this.att_datetime = "";
        this.att_location = "";
        this.lib_deviceID = "";
        this.lib_datetime = "";
        this.lib_loanRecordID = "";
        this.lib_loanStatus = "";
        this.park_lotID = "";
        this.park_deviceID="";
        this.park_datetime="";
        this.park_type="";
        this.park_mode="";

    }

    public String getCardHolderID() {
        return cardHolderID;
    }

    public void setCardHolderID(String cardHolderID) {
        this.cardHolderID = cardHolderID;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getDocExpDate() {
        return docExpDate;
    }

    public void setDocExpDate(String docExpDate) {
        this.docExpDate = docExpDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public String getFullNameVN() {
        return fullNameVN;
    }

    public void setFullNameVN(String fullNameVN) {
        this.fullNameVN = fullNameVN;
    }

    public String getAtt_deviceID() {
        return att_deviceID;
    }

    public void setAtt_deviceID(String att_deviceID) {
        this.att_deviceID = att_deviceID;
    }

    public String getAtt_datetime() {
        return att_datetime;
    }

    public void setAtt_datetime(String att_datetime) {
        this.att_datetime = att_datetime;
    }

    public String getAtt_location() {
        return att_location;
    }

    public void setAtt_location(String att_location) {
        this.att_location = att_location;
    }

    public String getLib_deviceID() {
        return lib_deviceID;
    }

    public void setLib_deviceID(String lib_deviceID) {
        this.lib_deviceID = lib_deviceID;
    }

    public String getLib_datetime() {
        return lib_datetime;
    }

    public void setLib_datetime(String lib_datetime) {
        this.lib_datetime = lib_datetime;
    }

    public String getLib_loanRecordID() {
        return lib_loanRecordID;
    }

    public void setLib_loanRecordID(String lib_loanRecordID) {
        this.lib_loanRecordID = lib_loanRecordID;
    }

    public String getLib_loanStatus() {
        return lib_loanStatus;
    }

    public void setLib_loanStatus(String lib_loanStatus) {
        this.lib_loanStatus = lib_loanStatus;
    }

    public String getPark_lotID() {
        return park_lotID;
    }

    public void setPark_lotID(String park_lotID) {
        this.park_lotID = park_lotID;
    }

    public String getPark_deviceID() {
        return park_deviceID;
    }

    public void setPark_deviceID(String park_deviceID) {
        this.park_deviceID = park_deviceID;
    }

    public String getPark_datetime() {
        return park_datetime;
    }

    public void setPark_datetime(String park_datetime) {
        this.park_datetime = park_datetime;
    }

    public String getPark_type() {
        return park_type;
    }

    public void setPark_type(String park_type) {
        this.park_type = park_type;
    }

    public String getPark_mode() {
        return park_mode;
    }

    public void setPark_mode(String park_mode) {
        this.park_mode = park_mode;
    }
}
